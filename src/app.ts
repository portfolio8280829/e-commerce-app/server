import dotenv from "dotenv";
import "express-async-errors";
dotenv.config();

import compression from "compression";
import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import fileUpload from "express-fileupload";
import ExpressMongoSanitize from "express-mongo-sanitize";
import helmet from "helmet";
import path from "path";
import connectDb from "./db/connectDb";

// ? Routes

import AuthRouter from "./routes/auth.route";
import CartRouter from "./routes/cart.route";
import OrderRouter from "./routes/order.route";
import ProductRouter from "./routes/product.route";
import ReviewRouter from "./routes/review.route";
import UserRouter from "./routes/user.route";

// ? End - Routes

// ? Middlewares

import ErrorHandlerMiddleware from "./middlewares/error-handler";
import NotFoundMiddleware from "./middlewares/notfound";

// ? End - Middlewares

const app = express();

app.use(
  cors({
    credentials: true,
    origin: true,
    optionsSuccessStatus: 200,
  })
);

app.use(cookieParser(process.env.JWT_SECRET!));
app.use(helmet());
app.use(ExpressMongoSanitize());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/", express.static(path.join(__dirname, "public")));
app.use(
  fileUpload({
    useTempFiles: true,
    // Maximum 5mb
    limits: { fileSize: 50 * 1024 * 1024 },
  })
);

app.use(compression());

// ? Routes initialization
app.use("/api/v1/users", UserRouter);
app.use("/api/v1/auth", AuthRouter);
app.use("/api/v1/products", ProductRouter);
app.use("/api/v1/orders", OrderRouter);
app.use("/api/v1/reviews", ReviewRouter);
app.use("/api/v1/cart", CartRouter);

// ? Middleware initialization

app.use(NotFoundMiddleware);
app.use(ErrorHandlerMiddleware);

const PORT = process.env.PORT! || "3500";

const start = async () => {
  try {
    await connectDb(process.env.MONGO_ATLAS_URI!);
    app.listen(PORT, () => console.log(`Server is listening to port ${PORT}`));
  } catch (error) {
    console.log(error);
  }
};

start();
