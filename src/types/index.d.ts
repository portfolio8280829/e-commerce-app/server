import { Types } from "mongoose";
import { Request } from "express";

export interface IUser extends Request {
  user?: any;
}

export type CustomErrorContent = {
  message: string;
};
