import { config } from "./nodemailerConfig";
import nodemailer from "nodemailer";

export interface IEmail {
  to: string;
  subject: string;
  html: string;
}

const sendEmail = async ({ to, subject, html }: IEmail) => {
  const transporter = nodemailer.createTransport(config);

  return transporter.sendMail({
    from: "Adors Tea <joarparrenopenaflorida@gmail.com>",
    to,
    subject,
    html,
  });
};

export default sendEmail;
