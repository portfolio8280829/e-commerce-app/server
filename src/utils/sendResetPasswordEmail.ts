import sendEmail from "./sendEmail";

export interface IResetEmail {
  username: string;
  email: string;
  token: string;
  origin: string;
}

const sendResetPasswordEmail = async ({
  username,
  email,
  token,
  origin,
}: IResetEmail) => {
  const resetURL = `${origin}/auth/reset-password?token=${token}&email=${email}`;

  const message = `<p>To reset your password, click on the following link : </p>
  <a href="${resetURL}">Reset Password</a></p> <br/>

  <p>Or copy and paste the URL into your browser : </p>
  <a href="${resetURL}">${resetURL}</a></p><br/>
  <p>Didn’t request a password reset? You can ignore this message.</p>;
  `;

  return sendEmail({
    to: email,
    subject: `Reset Password`,
    html: `<h3>Hello, ${username}</h3>
    <p>Forgot your password?</p>
    <p>We received a request to reset the password for your account.</p>
    <br/>
    <p>${message}</p>
    <br/><br/>
    <strong>Welcome to Adors Tea!</strong> <br/>
    <i>Adors Tea - All Rights Reserved</i>
    `,
  });
};

export default sendResetPasswordEmail;
