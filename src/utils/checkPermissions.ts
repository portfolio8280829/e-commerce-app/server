import * as CustomError from "../errors";

export interface IResource {
  role: string;
  userId: string;
}

const checkPermissions = (requestUser: IResource, resourceUserId: string) => {
  if (requestUser.role === "admin") return;
  if (requestUser.userId === resourceUserId.toString()) return;
  throw new CustomError.UnAuthorizedError(
    "Not authorized to access this route"
  );
};

export default checkPermissions;
