import crypto from "crypto";

const SECRET = "TODO_API";

export const random = () => {
  const resetId = crypto.randomBytes(32).toString("hex");
  const resetIdEscaped = encodeURIComponent(resetId);
  return resetIdEscaped;
};

export const authentication = (
  salt: string | undefined | null,
  password: string
) => {
  return crypto
    .createHmac("sha256", [salt, password].join("/"))
    .update(SECRET)
    .digest("hex");
};
