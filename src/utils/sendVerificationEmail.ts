import sendEmail from "./sendEmail";

export interface IVerifyEmail {
  username: string;
  email: string;
  verificationToken?: string | null | undefined;
  origin: string;
}
const sendVerificationEmail = async ({
  username,
  email,
  verificationToken,
  origin,
}: IVerifyEmail) => {
  const verifyEmail = `${origin}/auth/verify-email?token=${verificationToken}&email=${email}`;

  const message = `<p>Please confirm your email by clicking on the following link :
  <a href="${verifyEmail}">Verify Email</a> </p> <br/>

  <p>Or copy and paste the URL below into your browser : </p>
  <a href="${verifyEmail}">${verifyEmail}</a></p>
  `;

  return sendEmail({
    to: email,
    subject: "Email Verification",
    html: `<h3>Hi, ${username}</h3>

    <pre>We're happy you signed up for E-Commerce Website. To start exploring
      our community, please confirm your email address.
    </pre>
    <br/>
    ${message}
    <br/>
    <strong>Welcome to Adors Tea!</strong> <br/>
    <i>Adors Tea - All Rights Reserved</i>
    `,
  });
};

export default sendVerificationEmail;
