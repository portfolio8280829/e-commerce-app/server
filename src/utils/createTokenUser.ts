import { Types } from "mongoose";

interface IUser {
  username: string;
  _id: Types.ObjectId;
  role: string;
}

const createTokenUser = (user: IUser) => {
  return { username: user.username, userId: user._id, role: user.role };
};

export default createTokenUser;
