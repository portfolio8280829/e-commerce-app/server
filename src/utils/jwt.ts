import jsonwebtoken from "jsonwebtoken";
const { sign, verify } = jsonwebtoken;
import { Response } from "express";
import { checkPrimeSync } from "crypto";

interface ICookies {
  res: Response;
  user: Object;
  refreshToken: string;
}

const createJWT = ({ payload }: any) => {
  const token = sign(payload, process.env.JWT_SECRET!);
  return token;
};

const isTokenValid = (token: string) => {
  const data = verify(token, process.env.JWT_SECRET!);
  console.log(data);
  return data;
};

const attachCookiesToResponse = ({ res, user, refreshToken }: ICookies) => {
  const accessTokenJWT = createJWT({ payload: { user } });
  const refreshTokenJWT = createJWT({ payload: { user, refreshToken } });

  const oneDay = 1000 * 60 * 60 * 24;
  const longerExp = 1000 * 60 * 60 * 30;

  res.cookie("accessToken", accessTokenJWT, {
    httpOnly: false,
    secure: process.env.NODE_ENV === "production",
    signed: true,
    expires: new Date(Date.now() + oneDay),
  });

  res.cookie("refreshToken", refreshTokenJWT, {
    httpOnly: false,
    secure: process.env.NODE_ENV === "production",
    signed: true,
    expires: new Date(Date.now() + longerExp),
  });
};

export { createJWT, isTokenValid, attachCookiesToResponse };
