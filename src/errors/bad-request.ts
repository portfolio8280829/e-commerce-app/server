import { StatusCodes } from "http-status-codes";
import CustomAPIError from "./custom-error";

class BadRequestError extends CustomAPIError {
  statusCode = StatusCodes.BAD_REQUEST;
  constructor(message: string) {
    super(message || "Something went wrong, Please try again later.");
    Object.setPrototypeOf(this, BadRequestError.prototype);
  }
}

export default BadRequestError;
