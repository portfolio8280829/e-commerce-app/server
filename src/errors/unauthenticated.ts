import { StatusCodes } from "http-status-codes";
import CustomAPIError from "./custom-error";

class UnAuthenticatedError extends CustomAPIError {
  statusCode = StatusCodes.FORBIDDEN;
  constructor(message: string) {
    super(message || "Forbidden access");
    Object.setPrototypeOf(this, UnAuthenticatedError.prototype);
  }
}

export default UnAuthenticatedError;
