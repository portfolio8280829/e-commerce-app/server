import { StatusCodes } from "http-status-codes";
import CustomAPIError from "./custom-error";

class UnAuthorizedError extends CustomAPIError {
  statusCode = StatusCodes.UNAUTHORIZED;
  constructor(message: string) {
    super(message || "Unauthorized access");
    Object.setPrototypeOf(this, UnAuthorizedError.prototype);
  }
}

export default UnAuthorizedError;
