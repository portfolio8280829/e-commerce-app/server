import { Response, Request } from "express";
import { IUser } from "../types";
import * as CustomError from "../errors";
import Cart from "../model/cart.model";
import Product from "../model/product.model";
import { StatusCodes } from "http-status-codes";
import mongoose from "mongoose";

const addToCart = async (req: IUser, res: Response) => {
  const { userId } = req.user;
  const { product, quantity, name, price, imageUrl } = req.body;

  if (!req.user) {
    throw new CustomError.UnAuthenticatedError(
      `Unauthorized to access this route`
    );
  }

  const isValidProduct = await Product.findOne({ _id: product });

  if (!isValidProduct) {
    throw new CustomError.NotFoundError(
      `No product item with id ${product} found`
    );
  }

  let cart = await Cart.findOne({ user: userId });

  if (cart) {
    let itemIdx = cart?.cartItems.findIndex(
      (p) => p.product.toString() === product.toString()
    );

    if (itemIdx > -1) {
      let productItem = cart.cartItems[itemIdx];
      productItem.quantity = quantity;
      cart.cartItems[itemIdx] = productItem;
    } else {
      cart.cartItems.push({ product, quantity, name, price, imageUrl });
    }

    cart = await cart.save();
    return res.status(StatusCodes.OK).json({ product });
  } else {
    const newCart = await Cart.create({
      user: userId,
      cartItems: [{ product, quantity, name, price, imageUrl }],
    });
    return res.status(StatusCodes.OK).json(newCart);
  }
};

const removeToCart = async (req: IUser, res: Response) => {
  if (!req.user) {
    throw new CustomError.UnAuthenticatedError(
      `Unauthorized to access this route`
    );
  }

  const { userId } = req.user;
  const { id: cid } = req.params;

  const deletedCartItem = await Cart.findOneAndUpdate(
    { user: userId },
    { $pull: { cartItems: { _id: cid } } }
  );

  if (!deletedCartItem) {
    throw new CustomError.NotFoundError(`No cart item with id ${cid} found`);
  }

  res
    .status(StatusCodes.OK)
    .json({ msg: "Cart Item removed from cart", deletedCartItem });
};

const getAllUserCartItems = async (req: IUser, res: Response) => {
  if (!req.user) {
    throw new CustomError.UnAuthenticatedError(
      `Unauthorized to access this route`
    );
  }
  const { userId } = req.user;
  const cart = await Cart.find({ user: userId });

  res.status(StatusCodes.OK).json({ cart });
};

export { addToCart, removeToCart, getAllUserCartItems };
