import Order from "../model/order.model";
import Product from "../model/product.model";

import { StatusCodes } from "http-status-codes";
import * as CustomError from "../errors";
import { checkPermissions } from "../utils";
import { Request, Response } from "express";
import { IUser } from "../types";
import QRCode from "qrcode";

const fakeStripeAPI = async ({ amount, currency }: any) => {
  const client_secret = "someRandomValue";
  return { client_secret, amount };
};

const createOrder = async (req: IUser, res: Response) => {
  const { item: cartItems, tax, shippingFee } = req.body;

  if (!cartItems || cartItems.length < 1) {
    throw new CustomError.BadRequestError("No cart items provided");
  }
  if (!tax || !shippingFee) {
    throw new CustomError.BadRequestError(
      "Please provide tax and shipping fee"
    );
  }

  let orderItems: any[] = [];
  let subtotal = 0;

  for (const item of cartItems) {
    const dbProduct = await Product.findOne({ _id: item.product });

    if (!dbProduct) {
      throw new CustomError.NotFoundError(
        `No product with id : ${item.product}`
      );
    }

    const { inventory, name } = dbProduct;

    if (inventory < Number(item.quantity)) {
      throw new CustomError.BadRequestError(` ${name} Item is out of stock.`);
    }
  }

  for (const item of cartItems) {
    const dbProduct = await Product.findOne({ _id: item.product });

    if (!dbProduct) {
      throw new CustomError.NotFoundError(
        `No product with id : ${item.product}`
      );
    }

    const { name, price, imgUrl, _id } = dbProduct;

    const singleOrderItem = {
      quantity: Number(item.quantity),
      name,
      price: Number(price),
      imgUrl,
      product: _id,
    };
    // add item to order
    orderItems = [...orderItems, singleOrderItem];
    // calculate subtotal
    subtotal += Number(item.quantity) * Number(price);

    await Product.findOneAndUpdate(
      {
        _id: dbProduct,
      },
      { $inc: { inventory: -item.quantity } }
    );
  }
  // calculate total
  const total = Number(tax) + Number(shippingFee) + Number(subtotal);
  // get client secret
  const paymentIntent = await fakeStripeAPI({
    amount: total,
    currency: "usd",
  });

  const order = await Order.create({
    orderItems,
    total,
    subtotal,
    tax,
    shippingFee,
    clientSecret: paymentIntent.client_secret,
    user: req.user.userId,
  });

  res
    .status(StatusCodes.CREATED)
    .json({ order, clientSecret: order.clientSecret });
};

const getAllOrders = async (req: Request, res: Response) => {
  const orders = await Order.find({});

  res.status(StatusCodes.OK).json({ orders, count: orders.length });
};

const getSingleOrder = async (req: Request, res: Response) => {
  const { id: oid } = req.params;
  const order = await Order.findOne({ _id: oid });
  if (!order) {
    throw new CustomError.NotFoundError(`No order with id :  ${oid}`);
  }

  res.status(StatusCodes.OK).json({ order });
};

const getCurrentUserOrders = async (req: IUser, res: Response) => {
  const { userId } = req.user;
  const orders = await Order.find({ user: userId });

  res.status(StatusCodes.OK).json({ orders, count: orders.length });
};

const updateOrder = async (req: IUser, res: Response) => {
  const { id: oid } = req.params;
  const { paymentIntentId } = req.body;

  const order = await Order.findOne({ _id: oid });

  if (!order) {
    throw new CustomError.NotFoundError(`No order with id : ${oid}`);
  }

  checkPermissions(req.user, order.user.toString());

  order.paymentIntentId = paymentIntentId;
  order.status = "paid";
  await order.save();

  res.status(StatusCodes.OK).json({ order });
};

export {
  getAllOrders,
  getSingleOrder,
  getCurrentUserOrders,
  createOrder,
  updateOrder,
};
