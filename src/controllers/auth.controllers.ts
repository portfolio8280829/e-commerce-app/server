import crypto from "crypto";
import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import * as CustomError from "../errors";
import Token from "../model/token.model";
import User, { getUserByEmail } from "../model/user.model";
import { IUser } from "../types";

import {
  attachCookiesToResponse,
  authentication,
  createTokenUser,
  random,
  sendResetPasswordEmail,
  sendVerificationEmail,
} from "../utils";

// ? Route : auth/signup
// ? Signup
const signup = async (req: Request, res: Response) => {
  const { email, username, password } = req.body;

  if (!email || !password || !username) {
    throw new CustomError.BadRequestError("Invalid credentials");
  }

  const emailExists = await getUserByEmail(email);

  if (emailExists) {
    throw new CustomError.BadRequestError("Email address already exists");
  }

  const isFirstAccount = (await User.countDocuments({})) === 0;
  const role = isFirstAccount ? "admin" : "user";

  const verificationToken = random();

  const salt = random();
  const user = await User.create({
    username,
    email,
    role,
    authentication: {
      salt,
      password: authentication(salt, password),
      verificationToken,
    },
  });

  const origin = "http://localhost:3000";

  await sendVerificationEmail({
    username: user.username,
    email: user.email,
    verificationToken: user?.authentication?.verificationToken,
    origin,
  });

  res
    .status(StatusCodes.CREATED)
    .json({ msg: "Success! Please check your email to verify account" })
    .end();
};
// ? Route : auth/verify-email
// ? Verify Email Address

const verifyEmail = async (req: Request, res: Response) => {
  const { verificationToken, email } = req.body;

  const user = await getUserByEmail(email).select(
    "+authentication.verificationToken"
  );

  if (!user) {
    throw new CustomError.UnAuthenticatedError("Verification failed");
  }

  if (!user.authentication) return;

  if (user.authentication.verificationToken !== verificationToken) {
    throw new CustomError.UnAuthenticatedError(
      "Verification failed, Please check your email address"
    );
  }

  user.authentication.verificationToken = "";
  user.authentication.isVerified = true;
  user.authentication.verified = Date.now();

  await user.save();

  res.status(StatusCodes.OK).json({ message: "Email Address verified" }).end();
};

// ? Route : auth/signin
// ? Login

const signin = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  if (!email || !password) {
    throw new CustomError.BadRequestError("Please provide email and password");
  }

  const user = await getUserByEmail(email).select(
    "+authentication.salt +authentication.password"
  );

  if (!user) {
    throw new CustomError.UnAuthenticatedError("Invalid Credentials");
  }

  if (!user.authentication) return;

  const expectedHash = authentication(user?.authentication?.salt, password);

  if (user.authentication.password !== expectedHash) {
    throw new CustomError.UnAuthorizedError("Invalid Credentials");
  }

  if (!user.authentication.isVerified) {
    throw new CustomError.UnAuthenticatedError(
      "Please verify your email address"
    );
  }

  const tokenUser = createTokenUser(user);

  let refreshToken = "";

  const existingToken = await Token.findOne({ user: user._id });

  if (existingToken) {
    const { isValid } = existingToken;
    if (!isValid) {
      throw new CustomError.UnAuthenticatedError("Invalid Credentials");
    }
    refreshToken = existingToken.refreshToken;
    attachCookiesToResponse({ res, user: tokenUser, refreshToken });
    res.status(StatusCodes.OK).json({ user: tokenUser });
    return;
  }

  refreshToken = crypto.randomBytes(32).toString("hex");
  const userAgent = req.headers["user-agent"];
  const ip = req.ip;
  const userToken = { refreshToken, ip, userAgent, user: user._id };

  await Token.create(userToken);

  attachCookiesToResponse({ res, user: tokenUser, refreshToken });

  res.status(StatusCodes.OK).json({ user: tokenUser });
};

// ? Route : auth/forgot-password
// ? Forgot Password

const forgotPassword = async (req: Request, res: Response) => {
  const { email } = req.body;
  if (!email) {
    throw new CustomError.BadRequestError("Please provide valid email");
  }

  const user = await getUserByEmail(email).select(
    "+authentication.resetPasswordToken +authentication.resetPasswordTokenExpiry"
  );

  if (user) {
    const passwordToken = random();

    const origin = "http://localhost:3000";
    await sendResetPasswordEmail({
      username: user.username,
      email: user.email,
      token: passwordToken,
      origin,
    });

    const tenMinutes = 1000 * 60 * 10;
    const passwordTokenExpirationDate = new Date(Date.now() + tenMinutes);

    if (!user.authentication) return;
    user.authentication.resetPasswordToken = passwordToken;
    user.authentication.resetPasswordTokenExpiry = passwordTokenExpirationDate;
    await user.save();
  }

  res
    .status(StatusCodes.OK)
    .json({ msg: "Please check your email for reset password link" })
    .end();
};

// ? Route - auth/reset-password
// ? Reset Password

const resetPassword = async (req: Request, res: Response) => {
  const { token, email, password } = req.body;
  if (!token || !email || !password) {
    throw new CustomError.BadRequestError("Please provide all values");
  }
  const user = await getUserByEmail(email).select(
    "+authentication.password  +authentication.salt +authentication.resetPasswordToken +authentication.resetPasswordTokenExpiry"
  );

  if (!user) {
    throw new CustomError.BadRequestError(
      `No user with email address ${email} found`
    );
  }
  if (user) {
    const currentDate = new Date();

    if (
      !user.authentication ||
      !user.authentication.resetPasswordToken ||
      !user.authentication.resetPasswordTokenExpiry
    )
      return;

    if (
      token === user.authentication.resetPasswordToken &&
      user.authentication.resetPasswordTokenExpiry > currentDate
    ) {
      const salt = random();

      user.authentication.salt = salt;
      user.authentication.password = authentication(salt, password);
      user.authentication.resetPasswordToken = "";
      user.authentication.resetPasswordTokenExpiry = undefined;
      await user.save();
    }
  }

  res.status(StatusCodes.OK).json({ msg: "Reset Password Success" }).end();
};

// ? Route - auth/logout
// ? Logout

const logout = async (req: IUser, res: Response) => {
  await Token.findOneAndDelete({ user: req.user.userId });

  res.cookie("accessToken", "logout", {
    httpOnly: true,
    expires: new Date(Date.now()),
  });

  res.cookie("refreshToken", "logout", {
    httpOnly: true,
    expires: new Date(Date.now()),
  });

  res.status(StatusCodes.OK).json({ msg: "User logged out!" });
};

export { signin, forgotPassword, signup, verifyEmail, resetPassword, logout };
