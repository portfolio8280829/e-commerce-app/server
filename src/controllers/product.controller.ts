import Product from "../model/product.model";
import { StatusCodes } from "http-status-codes";
import * as CustomError from "../errors";
import path from "path";
import { IUser } from "../types";
import { Response, Request } from "express";
import { v2 as cloudinary } from "cloudinary";
import fileUpload from "express-fileupload";

const createProduct = async (req: IUser, res: Response) => {
  let file = req?.files?.image as fileUpload.UploadedFile;
  const {
    name,
    description,
    category,
    colors,
    featured,
    freeShipping,
    inventory,
    price,
  } = req.body;

  if (
    !name ||
    !description ||
    !category ||
    colors.length === 0 ||
    !featured ||
    !freeShipping ||
    !inventory ||
    !price
  ) {
    throw new CustomError.BadRequestError("Please provide all required fields");
  }

  const result = await cloudinary.uploader.upload(file.tempFilePath, {
    quality: 100,
    width: 800,
    height: 800,
    public_id: `${Date.now() + Math.floor(Math.random() * 1e4)}`,
    resource_type: "image",
    folder: "uploads/products/",
    format: "png",
  });

  const productObj = {
    imgUrl: result.secure_url,
    public_id: result.public_id,
    name,
    description,
    category,
    colors,
    featured,
    freeShipping,
    inventory,
    price,
    user: req.user.userId,
  };

  const product = await Product.create(productObj);
  res.status(StatusCodes.CREATED).json({ product });
};

const getSingleProduct = async (req: Request, res: Response) => {
  const { id: pid } = req.params;

  const product = await Product.findOne({ _id: pid }).populate("reviews");

  if (!product) {
    throw new CustomError.NotFoundError(`No product with id : ${pid}`);
  }

  res.status(StatusCodes.OK).json({ product });
};

const getAllProducts = async (req: Request, res: Response) => {
  const products = await Product.find({}).populate("reviews");

  res.status(StatusCodes.OK).json({ products, count: products.length });
};

const updateProduct = async (req: Request, res: Response) => {
  const { id: pid } = req.params;

  const product = await Product.findOne({ _id: pid }, req.body, {
    new: true,
    runValidators: true,
  });
  if (!product) {
    throw new CustomError.NotFoundError(`No product with id : ${pid}`);
  }

  res.status(StatusCodes.OK).json({ product });
};

const deleteProduct = async (req: Request, res: Response) => {
  const { id: pid } = req.params;

  const product = await Product.findOne({ _id: pid });
  if (!product) {
    throw new CustomError.NotFoundError(`No product with id : ${pid}`);
  }

  await product.deleteOne();
  res.status(StatusCodes.OK).json({ msg: "Success! Product removed." });
};

const uploadImage = async (req: Request, res: Response) => {};

export {
  createProduct,
  getAllProducts,
  getSingleProduct,
  updateProduct,
  deleteProduct,
  uploadImage,
};
