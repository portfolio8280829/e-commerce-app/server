import * as CustomError from "../errors";
import { Request, Response } from "express";
import User, {
  deleteUserById,
  getUserById,
  getUsers,
  updateUserById,
} from "../model/user.model";
import { IUser } from "../types";
import { StatusCodes } from "http-status-codes";
import { v2 as cloudinary } from "cloudinary";
import fileUpload from "express-fileupload";

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
  secure: true,
});

export const getAllUsers = async (req: Request, res: Response) => {
  const users = await getUsers();
  res.status(StatusCodes.OK).json(users).end();
};

export const getUser = async (req: IUser, res: Response) => {
  const { id: uid } = req.params;
  const user = await getUserById(uid);
  if (!user) {
    throw new CustomError.BadRequestError(`No user found with id ${uid}`);
  }
  res.status(StatusCodes.OK).json({ user });
};

export const deleteUser = async (req: Request, res: Response) => {
  const { id: uid } = req.params;
  const deleteUser = await deleteUserById(uid);
  if (!deleteUser) {
    throw new CustomError.BadRequestError(`No user found with id ${uid}`);
  }
  res
    .status(StatusCodes.OK)
    .json({ msg: "User Account Deleted", user: deleteUser })
    .end();
};

export const updateUserRole = async (req: Request, res: Response) => {
  const { id: uid } = req.params;
  const { username, role } = req.body;
  const user = await getUserById(uid);

  if (!user) {
    throw new CustomError.NotFoundError(`No user with id : ${uid}`);
  }
  const updatedUser = await updateUserById(uid, { username, role });

  res
    .status(StatusCodes.OK)
    .json({ msg: "User account updated", user: updatedUser })
    .end();
};

export const updateUserProfile = async (req: Request, res: Response) => {
  const { id: uid } = req.params;
  let file = req?.files?.image as fileUpload.UploadedFile;
  const user = await getUserById(uid);

  const { firstName, lastName, address, city, town, zipCode, country, phone } =
    req.body;

  const maxSize = 50 * 1024 * 1024;

  if (!user) {
    throw new CustomError.NotFoundError(`No user with id : ${uid}`);
  }
  if (!file) {
    throw new CustomError.BadRequestError("No file selected");
  }
  if (file.size > maxSize) {
    throw new CustomError.BadRequestError("File size must be less than 5mb");
  }
  if (!file.mimetype.startsWith("image")) {
    throw new CustomError.BadRequestError("Please upload image file only");
  }

  if (user?.userInformation?.imgURL && user?.userInformation?.public_id) {
    await cloudinary.uploader.destroy(
      user!.userInformation!.public_id.toString()
    );
  }

  const result = await cloudinary.uploader.upload(file.tempFilePath, {
    quality: 60,
    width: 500,
    height: 500,
    public_id: `${Date.now() + Math.floor(Math.random() * 1e4)}`,
    resource_type: "image",
    overwrite: true,
    folder: "uploads/user/",
    format: "jpg",
  });

  if (!result) {
    throw new CustomError.BadRequestError("Failed to upload image");
  }

  if (
    !firstName ||
    !lastName ||
    !address ||
    !city ||
    !town ||
    !zipCode ||
    !country ||
    !phone
  ) {
    throw new CustomError.BadRequestError(
      "Please fill in all user profile information"
    );
  }

  if (!user.userInformation) return;

  user.userInformation.firstName = firstName;
  user.userInformation.lastName = lastName;
  user.userInformation.address = address;
  user.userInformation.city = city;
  user.userInformation.town = town;
  user.userInformation.zipCode = zipCode;
  user.userInformation.country = country;
  user.userInformation.imgURL = result.secure_url;
  user.userInformation.public_id = result.public_id;
  user.userInformation.phone = phone;

  await user.save();

  res.status(StatusCodes.OK).json({ msg: "User profile updated", user }).end();
};

export const showUser = async (req: IUser, res: Response) => {
  console.log("Hello");
  res.status(StatusCodes.OK).json({ user: req.user });
};
