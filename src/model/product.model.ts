import mongoose, { InferSchemaType, model } from "mongoose";

const ProductSchema = new mongoose.Schema(
  {
    imgUrl: String,
    public_id: String,
    name: {
      type: String,
      required: [true, "Name is required"],
      minlength: 4,
      maxlength: 25,
    },
    description: {
      type: String,
      required: [true, "Please provide product description"],
      maxlength: [2500, "Description can not be more than 1000 characters"],
    },
    category: {
      type: String,
      enum: ["shake", "milktea", "tea", "frappe", "coffee"],
      default: "shake",
    },
    colors: {
      type: [String],
      default: ["#222"],
      required: true,
    },
    featured: {
      type: Boolean,
      default: false,
    },
    freeShipping: {
      type: Boolean,
      default: false,
    },
    inventory: {
      type: Number,
      required: true,
      default: 15,
    },
    price: {
      type: Number,
      required: [true, "Please provide product price"],
      default: 0,
    },
    averageRating: {
      type: Number,
      default: 0,
    },
    numOfReviews: {
      type: Number,
      default: 0,
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true,
    },
  },
  { timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } }
);

ProductSchema.virtual("reviews", {
  ref: "Review",
  localField: "_id",
  foreignField: "product",
  justOne: false,
});

ProductSchema.pre("save", async function (next) {
  await this.model("Review").deleteMany({ product: this._id });
});

const Product = mongoose.model("Product", ProductSchema);
export default Product;
