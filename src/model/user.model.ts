import mongoose, { InferSchemaType } from "mongoose";

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: [true, "Name is required"],
      minlength: 4,
      maxlength: 25,
    },
    email: {
      type: String,
      required: [true, "Email Address is required"],
      unique: true,
    },
    role: {
      type: String,
      enum: ["admin", "user"],
      default: "user",
    },

    userInformation: {
      imgURL: String,
      public_id: String,
      firstName: { type: String },
      middleName: { type: String },
      lastName: { type: String },
      address: { type: String },
      city: { type: String },
      town: { type: String },
      zipCode: { type: String },
      country: { type: String },
      phone: { type: String },
    },

    authentication: {
      password: {
        type: String,
        required: [true, "Password is required"],
        select: false,
      },
      salt: { type: String, select: false },
      verificationToken: { type: String, select: false },
      isVerified: Boolean,
      verified: { type: Number },

      resetPasswordToken: { type: String, select: false },
      resetPasswordTokenExpiry: { type: Date },

      lockedOut: Boolean,
      lockedOutExpiry: { type: Date },
    },
  },
  {
    timestamps: true,
  }
);

export const getUsers = () => User.find();

export const getUserByEmail = (email: string) => User.findOne({ email });

export const getUsersBySessionToken = (sessionToken: string) =>
  User.findOne({ "authentication.sessionToken": sessionToken });

export const getUserById = (id: string) => User.findById(id);

export const deleteUserById = (id: string) =>
  User.findOneAndDelete({ _id: id });

export const updateUserById = (id: string, values: Record<string, any>) =>
  User.findByIdAndUpdate(id, values, { new: true, runValidators: true });

const User = mongoose.model("User", UserSchema);

export default User;
