import express from "express";

import {
  authenticateUser,
  authorizePermissions,
} from "../middlewares/authentication";
import {
  createProduct,
  deleteProduct,
  getAllProducts,
  getSingleProduct,
  updateProduct,
} from "../controllers/product.controller";
const router = express.Router();

router
  .route("/")
  .get(authenticateUser, getAllProducts)
  .post([authenticateUser, authorizePermissions("admin"), createProduct]);

router
  .route("/:id")
  .get(authenticateUser, getSingleProduct)
  .patch([authenticateUser, authorizePermissions("admin")], updateProduct)
  .delete([authenticateUser, authorizePermissions("admin")], deleteProduct);

export default router;
