import express from "express";
import {
  authenticateUser,
  authorizePermissions,
} from "../middlewares/authentication";
import {
  getAllUsers,
  getUser,
  showUser,
  updateUserProfile,
  updateUserRole,
  deleteUser,
} from "../controllers/user.controller";

const router = express.Router();

router
  .route("/")
  .get([authenticateUser, authorizePermissions("admin")], getAllUsers)
  .patch([authenticateUser, authorizePermissions("admin")], updateUserRole);

router.get("/showCurrentUser", authenticateUser, showUser);

router
  .route("/:id")
  .get(authenticateUser, getUser)
  .patch(authenticateUser, updateUserProfile)
  .delete([authenticateUser, authorizePermissions("admin")], deleteUser);

export default router;
