import express from "express";

import { authenticateUser } from "../middlewares/authentication";

import {
  signup,
  signin,
  logout,
  verifyEmail,
  forgotPassword,
  resetPassword,
} from "../controllers/auth.controllers";

const router = express.Router();

router.post("/signup", signup);
router.post("/signin", signin);
router.post("/logout", authenticateUser, logout);
router.post("/verify-email", verifyEmail);
router.post("/forgot-password", forgotPassword);
router.post("/reset-password", resetPassword);

export default router;
