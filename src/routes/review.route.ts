import express from "express";
import { authenticateUser } from "../middlewares/authentication";
import {
  deleteReview,
  createReview,
  getAllReviews,
  getSingleReview,
  updateReview,
} from "../controllers/review.controller";

const router = express.Router();

router
  .route("/")
  .get(authenticateUser, getAllReviews)
  .post(authenticateUser, createReview);

router
  .route("/:id")
  .get(authenticateUser, getSingleReview)
  .patch(authenticateUser, updateReview)
  .delete(authenticateUser, deleteReview);

export default router;
