import AuthRoutes from "./auth.route";
import OrderRoutes from "./order.route";
import ProductRoutes from "./product.route";
import ReviewRoutes from "./review.route";
import UserRoutes from "./user.route";

export { AuthRoutes, OrderRoutes, ProductRoutes, ReviewRoutes, UserRoutes };
