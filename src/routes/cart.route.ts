import express from "express";

import { authenticateUser } from "../middlewares/authentication";

import {
  addToCart,
  getAllUserCartItems,
  removeToCart,
} from "../controllers/cart.controller";

const router = express.Router();

router
  .route("/")
  .post(authenticateUser, addToCart)
  .get(authenticateUser, getAllUserCartItems);

router.route("/:id").delete(authenticateUser, removeToCart);

export default router;
