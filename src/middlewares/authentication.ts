import { NextFunction, Request, Response } from "express";
import * as CustomError from "../errors";
import { isTokenValid, attachCookiesToResponse } from "../utils";

import Token from "../model/token.model";
import { IUser } from "../types";

interface IPayload {
  user: Object;
}

const authenticateUser = async (
  req: IUser,
  res: Response,
  next: NextFunction
) => {
  const { refreshToken, accessToken } = req.signedCookies;

  try {
    if (accessToken) {
      const payload: any = isTokenValid(accessToken);
      req.user = payload.user;
      console.log(req.user);
      return next();
    }

    const payload: any = isTokenValid(refreshToken);

    const existingToken = await Token.findOne({
      user: payload.user.userId,
      refreshToken: payload.refreshToken,
    });

    console.log(existingToken);
    if (!existingToken || !existingToken?.isValid) {
      throw new CustomError.UnAuthenticatedError("Authentication Invalid");
    }

    attachCookiesToResponse({
      res,
      user: payload.user,
      refreshToken: existingToken.refreshToken,
    });

    req.user = payload.user;
    next();
  } catch (error) {
    console.log(error);
    throw new CustomError.UnAuthenticatedError("Authentication Invalid");
  }
};

const authorizePermissions = (...roles: string[]) => {
  return (req: IUser, res: Response, next: NextFunction) => {
    if (!roles.includes(req.user.role)) {
      throw new CustomError.UnAuthorizedError(
        "Unauthorized to access this route"
      );
    }
    next();
  };
};

export { authorizePermissions, authenticateUser };
