import { Request, Response } from "express";

const NotFoundMiddleware = (req: Request, res: Response) =>
  res.status(404).send({ msg: "Route does not exists" });

export default NotFoundMiddleware;
